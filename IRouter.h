#pragma once

#include "IPort.h"

namespace serve
{
	class IRouter
	{
	public:
		virtual IPort * QueryPort(uint64_t) const = 0;
	};
}
