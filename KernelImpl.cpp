#include "pch.h"
#include "KernelImpl.h"

namespace serve
{
	CKernelImpl::CKernelImpl()
	{
		m_nUnknown = 10;
	}

	uint16_t CKernelImpl::Release()
	{
		if (this)
			delete this;

		return 0;
	}

	CKernelImpl* CKernelImpl::GetInstance()
	{
		return m_pInstance ? m_pInstance : m_pInstance = new CKernelImpl();
	}

	CKernelImpl* CKernelImpl::m_pInstance = nullptr;
}
