#include "pch.h"
#include "MsgRouter.h"

namespace serve
{
	CMsgRouter::CMsgRouter(int32_t serverCount)
	{
	}

	CMsgRouter::~CMsgRouter()
	{
	}

	IPort * CMsgRouter::QueryPort(uint64_t) const
	{
		return nullptr;
	}

	CMsgRouter * CMsgRouter::CreateNew()
	{
		auto pRouter = new CMsgRouter(3);

		if (!pRouter)
			return nullptr;

		return pRouter;
	}
}