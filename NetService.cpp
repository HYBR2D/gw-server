#include "pch.h"

#include "KernelImpl.h"

typedef serve::CKernelImpl * (__cdecl *NetServiceCreate)(int32_t*);

int main()
{
	const HINSTANCE hNetDLL = LoadLibrary(L"Net.dll");

	if (!hNetDLL)
	{
		std::cout << "Could not load Net.dll" << std::endl;
		return EXIT_FAILURE;
	}

	const NetServiceCreate netServiceCreate = reinterpret_cast<NetServiceCreate>(GetProcAddress(hNetDLL, "NetServiceCreate"));

	if (!netServiceCreate)
	{
		std::cout << "could not locate the function" << std::endl;
		return EXIT_FAILURE;
	}

	serve::CKernelImpl* pKernel = nullptr;

	int32_t test;

	pKernel = netServiceCreate(&test);

	return EXIT_SUCCESS;
}