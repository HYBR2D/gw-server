#pragma once

namespace serve
{
	class IPort
	{
	public:
		virtual int16_t GetPort();
		// virtual void* TakeMsg();
		// virtual void* SendMsg();
		// virtual void* TakeMsgFromOut();
		// virtual void* SendMsgToOut();
		// virtual void* CleanMsg();
		// virtual void* Stop();
		// virtual void* Start();
		// virtual void* DiscardMsg();

	private:
		int16_t m_nPort;
	};
}