#pragma once

#include "IPort.h"
#include "Serve.h"

namespace serve
{
	class CKernelImpl
	{
	public:
		CKernelImpl();
		~CKernelImpl() = default;
		
		virtual uint16_t Release();
		virtual IPort *QueryShellPort() { return nullptr; }
		virtual void CloseSocket(uint32_t) {}
		virtual void Init() {}
		virtual bool InitRouter() { return true; }
		virtual bool InitServe() { return true; }
		virtual void RunServes() {}
		virtual void DestroyServes() {}

		static CKernelImpl* GetInstance();

	private:
		int32_t m_nUnknown;
		int32_t m_nUnknown2;
		int32_t m_nUnknown3;
		int32_t m_nUnknown4;
		int32_t m_nUnknown5;
		int32_t m_nUnknown6;
		int32_t m_nUnknown7;

		std::map<uint16_t, CServe *> m_pServeList;
		std::map<uint64_t, std::string> m_pStringList;

		static CKernelImpl* m_pInstance;
	};
}
