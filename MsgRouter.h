#pragma once

#include "IRouter.h"

namespace serve
{
	class CMsgRouter : IRouter
	{
	public:
		CMsgRouter(int32_t serverCount);
		~CMsgRouter();

		IPort * QueryPort(uint64_t) const override;

		static CMsgRouter* CreateNew();
	};
}
