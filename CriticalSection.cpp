#include "pch.h"
#include "CriticalSection.h"

namespace sbase
{
	inline CCriticalSection::CCriticalSection()
	{
		InitializeCriticalSection(&m_pLock);
	}

	inline CCriticalSection::~CCriticalSection()
	{
		DeleteCriticalSection(&m_pLock);
	}

	void CCriticalSection::Lock()
	{
		EnterCriticalSection(&m_pLock);
	}

	void CCriticalSection::Unlock()
	{
		LeaveCriticalSection(&m_pLock);
	}
}