#pragma once

namespace sbase
{
	class CCriticalSection
	{
	public:
		inline CCriticalSection();
		virtual inline ~CCriticalSection();

		virtual void Lock();
		virtual void Unlock();

	private:
		CRITICAL_SECTION m_pLock;
	};
}